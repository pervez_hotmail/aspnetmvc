﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyUtils
{
    static class ListUtils
    {
        public static string Combine(this List<int> list, string delimiter)
        {
            return string.Join(delimiter, list);
        }
    }
}
