﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyUtils;
namespace LearnLanguage
{
    class Program
    {
        static void SayHello()
        {
            Console.WriteLine("Hello, World");
        }

        void SayHelloInstance()
        {
            Console.WriteLine("Hello, World");
        }

        public static void SayHelloWithArg(string name)
        {
            Console.WriteLine("Hello, " + name);
        }

        public static string SayHelloWithArgFunc(string name)
        {
            return "Hello, " + name;
        }

        static void Main(string[] args)
        {
            Action action = () => { Console.WriteLine("hello"); };
            action();

            Action<string> strAction = ( s => { Console.WriteLine("hello"); } );
            strAction("pervez");

            Func<string, string> strFunc = ( s => "hello" + s );

            Console.WriteLine(strFunc("pervez"));


            var list = new List<int> { 1, 2, 3 };
            Console.WriteLine(list.Combine(" "));

            var book = new Book { BookId = 1, Title="The C# Programming Language", Price = new Decimal(1.5) };
        }
    }
}
