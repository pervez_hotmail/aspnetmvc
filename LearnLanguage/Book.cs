﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LearnLanguage
{
    public class Book
    {
        public int BookId { get; set; }
        public string Title { get; set; }
        public decimal Price { get; set; }

        public Book(int bookId, string title, decimal price)
        {
            this.BookId = bookID;
            this.Title = title;
            this.Price = price;
        }

        public Book()
        {
        }
    }
}
